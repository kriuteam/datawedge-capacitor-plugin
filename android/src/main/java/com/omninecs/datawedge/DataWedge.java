package com.omninecs.datawedge;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import org.json.JSONArray;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@NativePlugin()
public class DataWedge extends Plugin {

    private BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // Bundle b = intent.getExtras();

            if (action.equals("com.dwbarcode.ACTION") && getSavedCall()!=null) {
                try {
                    String decodedData = intent.getStringExtra("com.symbol.datawedge.data_string");
                    JSObject ret = new JSObject();
                    ret.put("value", decodedData);
                    getSavedCall().success(ret);
                    freeSavedCall();
                } catch (Exception e) {

                }
            }
        }
    };


    @Override
    public void load() {
        super.load();

        IntentFilter filter = new IntentFilter();
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        filter.addAction("com.dwbarcode.ACTION");
        getContext().registerReceiver(myBroadcastReceiver, filter);
    }

    @Override
    protected void handleOnDestroy() {
        super.handleOnDestroy();
        getContext().unregisterReceiver(myBroadcastReceiver);
    }

    @PluginMethod()
    public void startScan(PluginCall call) {

        saveCall(call);

        Intent i = new Intent();
        i.setAction("com.symbol.datawedge.api.ACTION");
        i.putExtra("com.symbol.datawedge.api.ENABLE_DATAWEDGE", true);

        getContext().sendBroadcast(i);
    }

    @PluginMethod()
    public void stopScan(PluginCall call) {

        Intent i = new Intent();
        i.setAction("com.symbol.datawedge.api.ACTION");
        i.putExtra("com.symbol.datawedge.api.ENABLE_DATAWEDGE", false);

        getContext().sendBroadcast(i);
    }

    @PluginMethod()
    public void getMyIps(PluginCall call) {

        JSObject ret = new JSObject();
        JSONArray ips = new JSONArray();
        ret.put("ips", ips);

        HashMap<String,Boolean> ipUsed = new HashMap<String,Boolean>();

        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String ip = addr.getHostAddress();
                        int idx = ip.indexOf('%');
                        if(idx>0)
                            ip = ip.substring(0, idx);
                        if(!ipUsed.containsKey(ip)) {
                            ips.put(ip);
                            ipUsed.put(ip, true);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ret.put("error", ex.getMessage());
        }

        call.success(ret);
    }

}
