import { WebPlugin } from '@capacitor/core';
import { DataWedgePlugin } from './definitions';

export class DataWedgeWeb extends WebPlugin implements DataWedgePlugin {
  constructor() {
    super({
      name: 'DataWedge',
      platforms: ['web']
    });
  }

  async startScan(options: {}): Promise<any> {
    console.log('startScan web', options);
    return options;
  }

  async stopScan(options: {}): Promise<any> {
    console.log('stopScan web', options);
    return options;
  }

  async getMyIps(options: {}): Promise<any> {
    console.log('getMyIps web', options);
    return options;
  }

}

const DataWedge = new DataWedgeWeb();

export { DataWedge };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(DataWedge);
