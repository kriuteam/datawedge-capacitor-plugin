declare module "@capacitor/core" {
    interface PluginRegistry {
        DataWedge: DataWedgePlugin;
    }
}
export interface DataWedgePlugin {
    startScan(options: {}): Promise<any>;
    stopScan(options: {}): Promise<any>;
    getMyIps(options: {}): Promise<any>;
}
