import { WebPlugin } from '@capacitor/core';
import { DataWedgePlugin } from './definitions';
export declare class DataWedgeWeb extends WebPlugin implements DataWedgePlugin {
    constructor();
    startScan(options: {}): Promise<any>;
    stopScan(options: {}): Promise<any>;
    getMyIps(options: {}): Promise<any>;
}
declare const DataWedge: DataWedgeWeb;
export { DataWedge };
